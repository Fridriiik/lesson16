#include <iostream>
#include <ctime>

int GetDay() {
	time_t time_create = time(NULL);
	struct tm time_info;
	localtime_s(&time_info, &time_create);

	return time_info.tm_mday;
}
int main()
{
	const int N1 = 10;
	const int N2 = 5;

	int array[N1][N2]; 

	for (int i = 0; i < N1; i++) {
		for (int j = 0; j < N2; j++) {
			array[i][j] = i + j; 
			std::cout << array[i][j] << "\t";
		}
		std::cout << "\n"; 
	}


	
	const int Day = GetDay();
	const int Divider = 7;
	const int RowIndex = Day % Divider; 

	int ArraySum = 0;

	std::cout << "This day - " << Day << "\n"; 
	std::cout << "Day % Divider - " << Day % Divider << "\n";

	for (int j = 0; j < N2; j++) {
			ArraySum += array[RowIndex][j];
	}
	std::cout << ArraySum << "\n";

}

